%! TEX program = xelatex
\documentclass[a4paper]{article}

\usepackage{amsmath}
\usepackage{siunitx}
% See https://tex.stackexchange.com/a/82462/125609
\usepackage{caption}
\captionsetup[figure]{labelformat=empty}% redefines the caption setup of the figures environment in the beamer class.
\usepackage{titling}

% Links
\usepackage{hyperref}
\hypersetup{colorlinks = true,
	citecolor = gray,
	linkcolor = red,
	citecolor = green,
	filecolor = magenta,
	urlcolor = cyan
}
% Drawings and figures
\usepackage[american,siunitx]{circuitikz}
\usepackage{graphicx}
\usepackage{listings}
\lstset{basicstyle=\footnotesize,
	tabsize=4,
	keywordstyle=\color{blue},
	stringstyle=\color{red},
	commentstyle=\color{green},
	morecomment=[l][\color{magenta}],
}

%% the following commands are needed for some matlab2tikz features
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usetikzlibrary{plotmarks}
\usetikzlibrary{arrows.meta}
\usepgfplotslibrary{patchplots}
\usepackage{grffile}
\usetikzlibrary{calc,patterns,angles,quotes}

% Looks
\usepackage[top=1in,bottom=1in,inner=3cm,outer=2cm]{geometry}
% Provides commands to disable pagebreaking within a given vertical space. If
% there is not enough space between the command and the bottom of the page, a
% new page will be started.
\usepackage{needspace}
% For tables
\usepackage{booktabs}
\usepackage{color}
\usepackage{mathtools}
% Remove section and chapters numbers from headings -
% https://tex.stackexchange.com/questions/297265/remove-sections-numbering
\setcounter{secnumdepth}{0}

% Language
\usepackage{polyglossia}
\setdefaultlanguage{hebrew}
\setotherlanguage{english}
\usepackage{hebrewcal}

% Fonts
\setmainfont{DejaVu Serif}
\setsansfont{DejaVu Sans}
\setmonofont{DejaVu Sans Mono}
\newfontfamily\hebrewfont{Frank Ruehl CLM}[Script=Hebrew]
\newfontfamily\hebrewfontsf{FreeSerif}[Script=Hebrew]
\newfontfamily\hebrewfonttt{Liberation Mono}[Script=Hebrew]

% Fix for a parentheses issue with hebrew fonts + polyglossia, see
% https://tex.stackexchange.com/a/141437/125609
\makeatletter
\def\maketag@@@#1{\hbox{\m@th\normalfont\LRE{#1}}}
\def\tagform@#1{\maketag@@@{(\ignorespaces#1\unskip)}}
\makeatother

\begin{document}

\title{
	מעבדת פיזיקה 1מ \\
	התלות בין תדירות מתנד מאלץ לתנועה הרמונית לא מרוסנת
}
\author{\\
	דורון בכר \\
	ת.ז 316097872 \\
	יהב אורעד \\
	ת.ז 206223810 \\
}

\begin{titlingpage}
\maketitle
\begin{abstract}

מדדנו את תנועתה של עגלה על משטח חסר חיכוך בתור מודל לאוסילטור
הרמוני, לא מרוסן ומאולץ. העגלה הייתה מחוברת לשני קפיצים ולמנוע, כמאלץ. מדדנו את
מיקום העגלה כתלות בזמן ומדדנו את תדירות התנודות שלה. השווינו את תוצאה זו עם
התוצאה מהמודל התיאורטי לתנועת מתנד הרמוני מאולץ וקיבלנו שהמודל מנבא טוב מאוד את
אופי התנועה ואת תדירותה.

\end{abstract} \end{titlingpage}

\section{מבוא}

\subsection{המודל התיאורטי}

בהינתן מסה $m [kg]$, שמחוברת לקפיצים עם קבועים $k_1 [N/m]$ ו-$k_2 [N/m]$ מצד
אחד, ולאובייקט אחר שמפעיל כוח מהצורה:

\begin{equation} \label{eq:force-form}
	F(t) = F_0 \cos(\omega t)
\end{equation}

כאשר $\omega [1/s]$ היא תדירות התנודות של האובייקט, בהנחה שאין חיכוך, מתקבל
שמיקום המסה כתלות בזמן הוא:

\begin{equation} \label{eq:peimot}
	X(t) = \frac{F_0}{m (\omega_0^2-\omega^2)} \cdot (-2 \sin(\frac{\omega - \omega_0}{2} \cdot t) \cdot \sin(\frac{\omega + \omega_0}{2} \cdot t))
\end{equation}

כאשר $\omega_0 = \sqrt{\frac{m}{k_1 + k_2}}$.

בגבול $\omega \rightarrow \omega_0$, מתקבל:

\begin{equation} \label{eq:resonance}
	X(t) = \frac{F_0}{2 m \omega_0} \cdot \sin(\omega_0 \cdot t) \cdot t
\end{equation}

החיכוך במערכת תלוי בפרמטר $\tau [s]$, שערכו הוא הזמן שלוקח למערכת לדעוך ממשרעת
מקסימלית ל- $\frac{1}{e}$ מְערכה הראשוני. מפתרון של משוואת הכוחות כאשר מתייחסים
לחיכוך, מתקבל הקשר:

\begin{equation} \label{eq:phi-tau}
	\phi = \arctan(\frac{-\omega}{\tau (\omega_0^2 - \omega^2)})
\end{equation}

כאשר $\phi$ זה הפרש הפאזה בין הגל של מיקום המסה, לגל של מיקום המאלץ. עבור
$\tau$ סופי, אם $\omega \rightarrow \omega_0$ אז: $\phi \rightarrow
\frac{\pi}{2}$.

\subsection{אופן המדידה}

בנינו מתנד הרמוני, באמצעות עגלה שהורכבה על מסילה וכשהיא מחוברת לשני קפיצים משני
צידיה, ובאחד הצדדים הרכבנו עגלה נוספת שפעלה כמתנד, והיא הייתה האובייקט המאלץ,
כמתואר בתמונה:

\begin{figure}[h]
	\includegraphics[width=0.5\textwidth]{schematic-fig.png}
\caption{מערך הניסוי}
\label{fig:schematic-illustration}
\end{figure}

העגלה המאלצת זזה בתדירות של המנוע. שהיא $\omega$ במודל התאורטי.  המסילה בה
השתמשנו כמעט חסרת חיכוך כי היא מוציאה אוויר מלמטה כדי לגרום לעגלה לרחף (החיכוך
היחיד שנשאר הוא עם האוויר). על העגלה ישנן שיניים צפופות מאוד שבאמצעותן מדדנו את
מיקומה של העגלה עם גלאי פוטואלקטרי \textenglish{"Kruze"} שספר את כמות השיניים
לפיהם העגלה זזה ימינה ושמאלה. כך מדדנו את מיקומה של העגלה בזמן.

את קבועי הקפיץ מדדנו באמצעות תליית משקולות עם מסה ידועה לקפיץ מלמטה ואת קצהו
השני של הקפיץ למוט. את המסה, מחוברת לקפיץ, הנחנו על מד משקל, וידענו מבעוד מועד
את המרחק בין האורך הרפוי של הקפיץ לאורך אליו הוא הגיע עד למטה בגלל המשקולת.
באמצעות השינוי באורך הקפיץ והשינוי במדד משקל המסה, חישבנו את קבוע הקפיץ.

% dk(delta_x, delta_m, x, dm, g) = (symfun)
% 
%         _____________________
%        ╱   2  2     2   2  2
%       ╱  δₘ ⋅g    δₓ ⋅dm ⋅g
%      ╱   ────── + ──────────
%     ╱       2          4
%   ╲╱       x          x
% 
% SPRING1_K = (sym) 3.3549677182235833891424113442824
% SPRING2_K = (sym) 6.6100852067381314841359253022962
% FREQENCY0_MAGNET = (sym) 1.0987096021388957991204053532671
% DELTA_K1 = (sym) 0.0099850229709035210249074506641205
% DELTA_K2 = (sym) 0.0099850229709035210249074506641205

למד המשקל יש שגיאה של $\pm 0.1 [g]$, ומחוק הוק לחישוב קבוע קפיץ,
שאומר $ k = \frac{\Delta m g}{\Delta x} $, התקבל ש: $k_1 = 3.35 \pm 0.01$
ו-$k_2 = 6.61 \pm 0.01$.\footnote{חישוב השגיאה התבצע
\href{https://gitlab.com/doronbehar/physics1m-lab-harmonic-oscillator/blob/master/DRIVEN/main.m\#L15-28}{כאן}.}

\clearpage

\section{תוצאות הניסוי}

כיוונו את תדירות סיבוב המנוע,  וחיפשנו את תדירות התהודה. בהסתמך על התאוריה,
הנחנו שכאשר המערכת בתהודה, האמפליטודה המקסימלית גדלה באופן לינארי בזמן. מצאנו
אותה, ובגרף זה אפשר לראות את ההתאמה הלינארית למשוואה \ref{eq:resonance}, ואת
ההתאמה לפונקציית המיקום הכללית (משוואה \ref{eq:peimot}).

\begin{figure}[h]
	\input{Resonance-Frequency.tex}
\label{fig:resonance-freq}
\end{figure}

ההתאמה לקו הלינארי התקבלה עם $R^2 = 0.99989$. לגבי שאר הגרפים
שהותאמו, למדידה זו ולבאות אחריה, אין לנו נתונים אובייקטיביים על איכות ההתאמה
לפונקציות מהמודל.\footnote{השתמשנו בפונקצייה \texttt{nonlin\_curvefit} המתועדת
\href{https://octave.sourceforge.io/optim/function/nonlin_curvefit.html}{כאן}.}

\clearpage

כאן תדר הפעולה של המנוע היה נמוך יותר מתדר התהודה שמצאנו קודם, (במדידה
שתוצאותיה מוצגות בגרף \ref{fig:resonance-freq}).

\begin{figure}[h]
	\input{Lower-Frequency.tex}
\label{fig:lower-freq}
\end{figure}

\clearpage

כאן תדר הפעולה של המנוע היה גבוה יותר מתדר התהודה:

\begin{figure}[h]
	\input{Higher-Frequency.tex}
\label{fig:higher-freq}
\end{figure}

\clearpage

\section{דיון בתוצאות}

\subsection{השוואה עם התיאוריה}

במדידת תדר התהודה, קיבלנו התאמה לינארית לקצוות המשרעת החיוביות בדיוק של $R^2 = 0.99989
$, וגם קיבלנו התאמה טובה למשוואה \ref{eq:resonance}, מבלי לקחת את
הגבול $\omega \rightarrow \omega_0$. ניתן גם להבחין שערכה של תדירות הפעימות
($\frac{\omega_0 - \omega}{2} \cdot \frac{1}{2 \pi}$), היה אפסי בהשוואה למדידות
האחרות.

בשביל לאמוד את החיכוך במערכת, ביצענו חישוב ידני של הפרש הפאזות בין תדר המתנד
לבין תדר המסה, זאת על פי משוואה \ref{eq:phi-tau}. מכיוון שבמערכת שלנו חייב
להיות חיכוך כלשהו, ובגלל שחיפשנו את תדר התהודה ($\omega \rightarrow \omega_0$),
ציפינו לקבל $\phi \rightarrow \frac{\pi}{2}$. בחישוב התקבל \textenglish{$\phi
\approx 68.686 [deg]$}, ערך שלא רחוק מ-$\frac{\pi}{2}$ בגדר הטווח שהוא
$(0,\pi)$. אף על פי כן, אם היה חיכוך שאינו זניח, בכלל לא היינו מקבלים פעימות
כמו במדידות \ref{fig:lower-freq} ו-\ref{fig:higher-freq}.  לכן, ניתן להסיק
שהחיכוך אכן זניח למטרות הניסוי הזה.

אין באפשרותנו להשוות את תדירות התהודה שהתקבלה במדידות, לתדירות התהודה התאורטית
מהניסוי הקודם, זאת כי בניסוי ההוא, המערכת הייתה שונה, עם $m [kg]$ ו- $k_1$,
$k_2$ ($ [N/m]$) שונים. לכן, לא נוכל להסיק מסקנה קונקרטית מהשוואה זו.

בשביל לאמוד את הרגישות שלנו למציאת תדר התהודה, נתבונן בתוצאות המוצגות בגרף
\ref{fig:resonance-freq}. ראשית נבחין כי התקבל מההתאמה למשוואה \ref{eq:peimot},
ש: $\frac{\omega_0 -\omega}{2} \cdot \frac{1}{2\pi}= 0.007041 [Hz]$ כלומר היינו
רחוקים לפי התאמה זו מתדר התהודה ב-$0.088480 [rad/s]$. ראוי לציין, שככל ש$\omega
\rightarrow \omega_0$, כך גם רוחבה של כל "פעימה", הולך וגדל. לכן תחת המגבלות
הטכניות של הניסוי, לא נוכל לזהות פעימה שלמה במדידה אחת בתדר תהודה, כי אורכה של
המסילה מגביל את המשרעת המקסימלית שאפשר לקבל. אף על פי כן, כן ניתן להעריך שתדר
התהודה שמצאנו אכן היה קרוב דיו ל$\omega_0$, כי ההתאמה לקצוות המשרעת הייתה אכן
לינארית בתחום אליו התייחסנו.

בנוסף, ניתן לאמוד את הקירוב שלנו ל-$\omega_0$ ע"' חישוב ערך זה באמצעות
התדירויות שמתקבלות מההתאמות לגרפים. כלומר, לכל התאמה, קיבלנו את ערכי
$\frac{\omega_0 - \omega}{2} \cdot \frac{1}{2\pi}$ ו-$\frac{\omega_0 +
\omega}{2} \cdot \frac{1}{2\pi}$. מפתרון שתי המשוואות ובידוד הנעלם $\omega_0$,
קיבלנו שערך ה-$\omega_0$ הממוצע שחושב מכל מההתאמות הינו $6.6557 [rad/s]$. ערך
זה קרוב מאוד לערכו של$\omega_0$ שחושב לפי המסה וקבועי הקפיץ והוא $6.9033\pm
0.005 [rad/s]$.\footnote{חישוב השגיאה הנגררת של $\omega_0$ לפי מדידות הקפיצים
בוצע
\href{https://gitlab.com/doronbehar/physics1m-lab-harmonic-oscillator/blob/master/DRIVEN/main.m\#L15-28}{כאן}.}

\subsection{שגיאות המדידה}

מכשיר המדידה היחיד שלקח חלק במדידות היה החיישן הפוטואלקטרי. הוא ספר כמה שיניים
ימינה ושמאלה העגלה עברה, בתדר דגימה מסוים. תדר הדגימה עבור המדידה הראשונה
(\ref{fig:resonance-freq}), היה 100 מדידות לשנייה ועבור שתי המדידות האחרות הוא
היה 200 מדידות לשנייה. בנוסף הייתה לו שגיאה של $\pm 1$ שיניים ימינה או שמאלה.
לכן סך הכל השגיאה שלו היא $\pm 1$ בציר ה-$Y$, ו-$\pm0.0005[s]$ ($\pm 0.001 [s]$
למדידה הראשונה) בציר ה-$X$. על הגרף לא ניתן היה להציג רווח בר סמך כי גודלו היה
קטן מידי.

\section{מסקנות}

המודל התאורטי ניבא טוב מאוד את אופי התנועה - קיבלנו תנועה מחזורית עם התאמה
מעולה לגל מחזורי בצורת סינוס. בנוסף, ניבוי התדר של המודל היה מוצלח למדי. אימתנו
שאכן המודל מתאר טוב מערכת עם חיכוך מינימלי כי מתקבלות פעימות כצפוי.

\section{נספח}

הקוד שנכתב לצורך עיבוד הנתונים, חישוב השגיאות, והנתונים עצמם נמצאים
\href{https://gitlab.com/doronbehar/physics1m-lab-harmonic-oscillator/blob/master/DRIVEN}{כאן}.

\end{document}
