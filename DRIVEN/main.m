#!/usr/bin/env octave

% Load package that fits non linear curves to data points
pkg load optim
% to findpeaks
pkg load signal

% Function to print stuff nicely
function print_measurements(m, name)
	disp("")
	disp(sprintf("@@@@@@@@@    %s   @@@@@@@@@", name))
	disp("")
	%% Define functions for later use in computing errors
	% constant
	EARTH_G = sym(9.78033, 'f');
	syms k(dm,x, g);
	k(dm,x, g) = (dm * g)/x;
	syms omega0(k1, k2, m1, m2);
	omega0(k1, k2, m1, m2) = sqrt((k1 + k2)/(m1 + m2));
	% To compute the error
	syms dk(delta_x, delta_m, x, dm, g);
	dk(delta_x, delta_m, x, dm, g) = sqrt((diff(k, x) * delta_x)^2 + (delta_m * diff(k, dm))^2);
	SPRING1_K = vpa(k(m.spring1_dmass, m.dx, EARTH_G))
	SPRING2_K = vpa(k(m.spring2_dmass, m.dx, EARTH_G))
	OMEGA0_NO_MAGNET = vpa(omega0(SPRING1_K, SPRING2_K, 0, m.wagon_mass))
	syms domega(delta_k, delta_m, k1, k2, m1, m2)
	domega(delta_k, delta_m, k1, k2, m1, m2) = sqrt( ...
		(diff(omega0, k1)*delta_k)^2 + ...
		(diff(omega0, k2)*delta_k)^2 + ...
		(diff(omega0, m1)*delta_m)^2 + ...
		(diff(omega0, m2)*delta_m)^2 ...
	)
	DELTA_M = sym(0.1*0.001, 'f');
	% We assume the length of the 97.95 is totally correct, because it came
	% that way from the supplier
	DELTA_X = sym(0, 'f')
	DELTA_K1 = vpa(dk(DELTA_X, DELTA_M, m.dx, m.spring1_dmass, EARTH_G))
	DELTA_K2 = vpa(dk(DELTA_X, DELTA_M, m.dx, m.spring2_dmass, EARTH_G))
	DELTA_OMEGA0 = domega(DELTA_K2, DELTA_M, SPRING1_K, SPRING2_K, sym(0, 'f'), m.wagon_mass)
end
m = struct();
% NOTE: m.dx should be the same for both DORON and YAHAV
m.dx = sym(97.95*0.001, 'f');
%% DORON MEASUREMENTS
m.spring1_dmass = sym(60.6*0.001, 'f');
m.spring2_dmass = sym(42.2*0.001, 'f');
m.wagon_mass = sym(207.4*0.001, 'f');
print_measurements(m, "DORON")
%% YAHAV MEASUREMENTS
m.spring1_dmass = sym(33.6*0.001, 'f');
m.spring2_dmass = sym(66.2*0.001, 'f');
m.wagon_mass = sym(209.1*0.001, 'f');
print_measurements(m, "YAHAV")

%
% Get data out of a file
%
function [counts, times] = get_xy_data(fname, channel)
	file = fopen(fname);
	% All measurements txt files have the same format
	data = textscan(file, '%d %f %f %f', 'HeaderLines', 1);
	fclose(file);
	% The 1st column is not interesting, it's just an index
	times = data{2};
	if strcmp(channel, "A")
		counts = data{3};
	elseif strcmp(channel, "B")
		counts = data{4};
	else
		error("unknown channel specified")
	end
end

%% Constant parametric function used throughout all fittings, using nonlin_curvefit
global harmonic_param_func;
harmonic_param_func = @(p, t) p(1).*(-2).*sin(2*pi*p(2)*t + p(5)).*sin(2*pi*p(3)*t + p(5)) + p(4);

function [p, model_values, cvg, outp, real_times, real_counts, range, peaks, peaks_i, beats_wave_phase] = fit_generic(counts, times, freq_type)
	% Bring it to scope
	global harmonic_param_func;
	switch freq_type
		case 'Resonance'
			% Not needed here
			beats_wave_phase = 0;
			[min_c, min_i] = min(counts);
			[max_c, max_i] = max(counts);
			% Where the actual interesting and best fitted data begin
			ioffset = 210;
			range = ioffset:max_i;
			real_times = times(range);
			real_counts = counts(range);
			[peaks, peaks_i] = findpeaks(max(real_counts, 0), 'MinPeakDistance', abs(max_i-min_i)/4);
			frequency = 1/(mean(diff(times(peaks_i))));
			amplitude = max_c;
			guessed_values_arr = [ ...
				amplitude; ...
				0; ...
				frequency; ...
				0;
				0;
				0;
			];
			% size(real_counts)
			% size(real_times)
			[p, model_values, cvg, outp] = nonlin_curvefit( ...
				harmonic_param_func, ...
				guessed_values_arr, ...
				real_times, ...
				real_counts ...
			);
			return
		case 'Lower'
			% Found by experimenting
			beats_wave_phase = 12.15;
			[min_c, min_i] = min(counts);
			[max_c, max_i] = max(counts);
			[peaks, peaks_i] = findpeaks(counts, 'MinPeakDistance', abs(max_i-min_i)/2, 'DoubleSided');
			ioffset = 8;
			range = peaks_i(ioffset):length(counts);
			real_counts = counts(range);
			real_times = times(range);
			[resonance_peaks, resonance_peaks_i] = findpeaks(counts, 'MinPeakDistance', length(times)/4, 'DoubleSided');
			harmonic_param_func = @(p, t) p(1).*sin(2*pi*p(2)*t + p(6)).*sin(2*pi*p(3)*t+p(5)) + p(4);
			resonance_frequency = 1/(mean(diff(times(resonance_peaks_i)))*2);
			frequency = 1/(mean(diff(times(peaks_i)))*2);
			amplitude = max_c;
			% TODO: Comment where this came from
			t_1 = times(resonance_peaks_i(1));
			t_2 = real_times(1);
			A = max_c;
			B = 0;
			resonance_phase = (A*t_2 - B*t_1)/(A-B);
			guessed_values_arr = [ ...
				amplitude; ...
				resonance_frequency; ...
				frequency; ...
				0; ...
				resonance_phase; ...
				0; ...
				0; ...
			];
			[p, model_values, cvg, outp] = nonlin_curvefit( ...
				harmonic_param_func, ...
				guessed_values_arr, ...
				real_times, ...
				real_counts ...
			);
			return
		case 'Higher'
			% Found by experimenting
			beats_wave_phase = 12.1;
			[min_c, min_i] = min(counts);
			[max_c, max_i] = max(counts);
			[peaks, peaks_i] = findpeaks(counts, 'MinPeakDistance', abs(max_i-min_i)/2, 'DoubleSided');
			range = peaks_i(2):length(counts);
			real_counts = counts(range);
			real_times = times(range);
			[resonance_peaks, resonance_peaks_i] = findpeaks(counts, 'MinPeakDistance', length(times)/4, 'DoubleSided');
			resonance_frequency = 1/(mean(diff(times(resonance_peaks_i)))*2);
			frequency = 1/(mean(diff(times(peaks_i)))*2);
			amplitude = max_c;
			guessed_values_arr = [ ...
				amplitude; ...
				resonance_frequency; ...
				frequency; ...
				0; ...
				0; ...
				0; ...
				0; ...
			];
			[p, model_values, cvg, outp] = nonlin_curvefit( ...
				harmonic_param_func, ...
				guessed_values_arr, ...
				real_times, ...
				real_counts ...
			);
			return
	end
end

function m = get_measurement(fname)
	fname_split = strsplit(fname, {"-", "."});
	[counts, times] = get_xy_data(fname, "B");
	% 1st part of file name is the frequency type used in fit_generic
	[p, model_values, cvg, outp, real_times, real_counts, range, peaks, peaks_i, beats_wave_phase] = fit_generic(counts, times, fname_split(1){1});
	freq_h = max(p(2), p(3));
	freq_l = min(p(2), p(3));
	m = struct( ...
		'fname', fname, ...
		% The range of indices for which the counts and times are at most interest
		'range', range, ...
		'sampling_rate', 1/(times(2)-times(1)), ...
		'times', times, ...
		'counts', counts, ...
		'peaks', peaks, ...
		'peaks_i', peaks_i, ...
		'by', fname_split(2), ...
		'total_counts', size(counts, 1), ...
		'freq_type', fname_split(1), ...
		'display_phase', beats_wave_phase, ...
		'fitResults', struct( ...
			'as_array', p, ...
			'amplitude', p(1), ...
			'amplitude_shift', p(4), ...
			'successful', cvg, ...
			'omega0', 2*pi*(freq_h - freq_l), ...
			'frequency', freq_h, ...
			'phase', 2*p(5), ...
			'resonance_freq', freq_l, ...
			'computed_values', model_values ...
		)
	);
end

function plotM(fig_h, m)
	data_h = plot(m.times(m.range), m.counts(m.range), '.');
	hold on;
	fit_h = plot(m.times(m.range), m.fitResults.computed_values, '-');
	xlabel("Time [s]");
	ylabel("offset of teeths");
	data_title = sprintf('Frequency Attempted: %s, By: %s, Sampling Rate: %d, Total Counts: %d', ...
		m.freq_type, ...
		m.by, ...
		m.sampling_rate, ...
		m.total_counts ...
	);
	% TODO: Print this to plot caption
	fit_title = sprintf('Driving Frequency: %f [hz], Beats Frequency: %f [hz]', ...
		m.fitResults.frequency, ...
		m.fitResults.resonance_freq ...
	);
	legend_titles = { ...
		'Mass Position', ...
		'Mass Position Fit', ...
	};
	legend_location = "north";
	% TODO: comment this
	if strcmp(m.freq_type, "Resonance")
		[counts, times] = get_xy_data("Resonance-Yahav.txt", "A");
		driving_force_param_func = @(p, t) p(1)*sin(2*pi*p(3)*t) + p(2)*cos(2*pi*p(4)*t) + p(5);
		[min_c, min_i] = min(counts);
		[max_c, max_i] = max(counts);
		[peaks, peaks_i] = findpeaks(counts, 'MinPeakDistance', abs(max_i-min_i)/6, 'DoubleSided');
		frequency = 1/(mean(diff(times(peaks_i)))*2);
		y_offset = (max(peaks) - min(peaks))/2;
		amplitude = mean(abs(diff(peaks))) - y_offset;
		guessed_values_arr = [ ...
			amplitude; ...
			amplitude; ...
			frequency; ...
			frequency; ...
			y_offset; ...
		];
		real_times = times(m.range);
		real_counts = counts(m.range);
		driving_h = plot(real_times, real_counts, '.');
		[p, fy, cvg, outp] = nonlin_curvefit( ...
			driving_force_param_func, ...
			guessed_values_arr, ...
			times, ...
			counts ...
		);
		plot(real_times, fy(m.range), '-')
		driving_amplitude = sqrt(p(1)^2 + p(2)^2);
		driving_phase = atan(p(2)/p(1));
		phase = (m.fitResults.phase - driving_phase) * (180/pi);
		RESONANCE_GRAPH_PHASE_DIFF = phase
		legend_titles{3} = "Driving Force";
		legend_titles{4} = "Driving Force Fit";
		% TODO: Print phase to caption of plot
		linear_times = m.times(m.peaks_i);
		linear_counts = m.peaks;
		[p_fit, regression_info] = polyfit(linear_times, linear_counts, 1);
		% m.times and m.counts are the times and counts that were truncated
		% from the original data, that's why when we'll attempt to plot the
		% polynomial fit, we'd want to shift it a bit, according to the range
		% which we know as well.
		line_offset = (m.range(1)-1) / m.sampling_rate;
		polyfit_func = @(x) p_fit(1) * (x - line_offset) + p_fit(2);
		plot_domain = [linear_times(1), linear_times(end)] + line_offset;
		fplot(polyfit_func, plot_domain);
		ss_res = regression_info.normr^2;
		y_bar = mean(linear_counts);
		ss_tot = sum(arrayfun(...
			@(y) (y - y_bar)^2, ...
			linear_counts ...
		));
		RESONANCE_GRAPH_LINEAR_FIT_R2 = 1 - ss_res/ss_tot
		legend_titles{5} = "Driving Force Linear Fit to Peaks";
		% comes out better because data is growing to the left
		legend_location = "northwest";
	else
		line_offset = (m.range(1)) / m.sampling_rate;
		imaginary_fit = @(t) m.fitResults.amplitude .* sin(2*pi*m.fitResults.resonance_freq*(t) + m.display_phase) + m.fitResults.amplitude_shift;
		plot_domain = [m.times(1), m.times(end)] + line_offset;
		fplot(imaginary_fit, plot_domain);
		legend_titles{3} = "Beats Fit";
	end
	legend(legend_titles, ...
		'Location', legend_location ...
	)
	legend('boxoff')
end

measurements = [ ...
	get_measurement("Resonance-Yahav.txt"); ...
	get_measurement("Lower-Yahav.txt"); ...
	get_measurement("Higher-Yahav.txt"); ...
];
MEASURED_OMEGA0 = mean([[measurements.fitResults].omega0])
fid = fopen("measured-omega0.tex",'w');
fprintf(fid,'$ %f [Hz]$', MEASURED_OMEGA0);
fclose(fid);
for m_i=1:length(measurements)
	plotM(figure(), measurements(m_i))
	plot_fname = sprintf("%s-Frequency.tex", measurements(m_i).freq_type);
	matlab2tikz( ...
		'filename', plot_fname, ...
		% To make the file lighter
		'externalData', true, ...
		'extraCodeAtEnd', sprintf( ...
			"\\caption{תדר התהודה שהתקבל בהתאמה זו היה $\\frac{\\omega_0 - \\omega}{2} \\cdot \\frac{1}{2 \\pi} = %f [Hz]$}", ...
			measurements(m_i).fitResults.resonance_freq
		), ...
		'showInfo', false ...
	)
	% Do something that's impossible using matlab2tikz's API, to make the dots lighter
	fid = fopen(plot_fname,'r');
	f=fread(fid,'*char')';
	fclose(fid);
	fid = fopen(plot_fname,'w');
	fprintf(fid,'%s',regexprep(f,'mark options={solid, ','mark options={solid,  mark size=0.4pt, '));
	fclose(fid);
end
