#!/usr/bin/env octave

% Load package that fits exp * cos() to data points
pkg load optim
% to findpeaks
pkg load signal
% TODO: explain what does this do
close all
% TODO: Remove when finished
% debug_on_warning()

%% Measurements
% This are in grams +- 0.1
spring1_dmass = sym(17.67*0.001, 'f');
spring2_dmass = sym(11.74*0.001, 'f');
wagon_mass = sym(209.2*0.001, 'f');
magnets_mass = sym(6.66*0.001, 'f');
dx = sym(97.95*0.001, 'f');
% constant
earth_g = sym(9.78033, 'f');
%% Computations
% Define functions
syms k(dmg,x);
k(dmg,x) = dmg/x;
syms omega0(k1, k2,m1,m2);
omega0(k1, k2,m1,m2) = sqrt((k1 + k2)/(m1 + m2));
% Use them to compute measured values
SPRING1_K = vpa(k(earth_g * spring1_dmass, dx))
SPRING2_K = vpa(k(earth_g * spring2_dmass, dx))
FREQENCY0_NO_MAGNET = vpa(omega0(SPRING1_K, SPRING2_K, 0, wagon_mass)/(2 *sym(pi)))
FREQENCY0_WITH_MAGNET = vpa(omega0(SPRING1_K, SPRING2_K, magnets_mass, wagon_mass)/(2 *sym(pi)))
% Compute the error
syms dk(delta_x, delta_mg, x, mg);
dk(delta_x, delta_mg, x, dmg) = sqrt((diff(k, x) * delta_x)^2 + (delta_mg * diff(k, dmg))^2)
% We assume the length of the 97.95 is totally correct
DELTA_K1 = vpa(dk(sym(0, 'f'), sym(0.1*0.001, 'f')*earth_g, dx, earth_g * spring1_dmass))
DELTA_K2 = vpa(dk(sym(0, 'f'), sym(0.1*0.001, 'f')*earth_g, dx, earth_g * spring2_dmass))

%
% Get data out of a file
%
function [counts, times] = get_xy_data(fname)
	file = fopen(fname);
	% All measurements txt files have the same format
	data = textscan(file, '%d %f %f', 'HeaderLines', 1);
	fclose(file);
	% The 1st column is not interesting, it's just an index
	counts = data{3}; times = data{2};
end

% pretty much a "constant" parametric function, used to fit the data to the
% observations.
global harmonic_param_func
harmonic_param_func = @(t, p) exp(-t * p(2)) .* (p(1)*sin(2*pi*p(3)*t) + p(4)*cos(2*pi*p(3)*t));
%
% We are going to use the optim package's nonlin_curvefit inside this function.
%
% https://octave.sourceforge.io/optim/function/leasqr.html
%
% This function returns this exactly same variables as output.
%
function [f, p, cvg, iter, corp, covp, covr, stdresid, Z, r2] = fit_generic_curve(counts, times)
	% Compute the initial values - according to the point where the speed was
	% minimal: When the right most column (count column) was at it's lowest
	% value, it's where we should have a zero speed
	[min_c, min_i] = min(counts);
	[max_c, max_i] = max(counts);
	% the maximum value is half of that of the real value because in parts
	% of the plot, the speed is "negative"
	max_amplitude = max_c/2;
	% The frequency is given roughly by 1 over 2*( the distance between the 2
	% first min - max )
	frequency = 1/(2 * abs(times(max_i) - times(min_i)));
	% We replace negative values with zeros in counts before passing them to
	% findpeaks, see:
	% https://www.mathworks.com/matlabcentral/answers/377607-how-to-replace-negative-elements-in-a-matrix-with-zeros
	[peaks, peaks_i] = findpeaks(max(counts, 0), 'MinPeakDistance', (max_i-min_i)/4);
	% We compute an initial guess for tau, based on the fact that as t grows, the
	% ratio between the every local maxima, is somewhat proportional to it
	dt = times(peaks_i(end) - peaks_i(1));
	k = peaks(1)/peaks(end);
	tau = dt/k;
	inversed_tau = 1/(2 *tau);

	% Fit the graph
	guessed_values_arr = [ ...
		max_amplitude; ...
		inversed_tau; ...
		frequency; ...
		% max_amplitude is a good estimation both for sin and cos coefficients
		max_amplitude ...
	];
	% Bring it to scope
	global harmonic_param_func
	[f, p, cvg, iter, corp, covp, covr, stdresid, Z, r2] = leasqr( ...
		times, ...
		counts, ...
		guessed_values_arr, ...
		harmonic_param_func, ...
		1e-9 ...
	);
end

% Get all properties of a measurement file according to the file name
measurements_files = dir('*.txt');
% See comment at else in strcmp part of loop
induced_index=0;
for fi=1:length(measurements_files)
	fname = measurements_files(fi).name;
	fname_split = strsplit(fname, {"-", "."});
	if strcmp(fname_split(1), "nomag")
		magnet = false;
		magnetOrientation = 'None';
	elseif strcmp(fname_split(1), "mag")
		magnet = true;
		magnetOrientation = fname_split(2);
	else
		% probably not a real measurement file - this means that our resulting
		% measurements array should have 1 index less because of this
		% irrelevant file. We use this induced_index vaiable when assigning
		% measurements the structure data in the correct index
		induced_index++;
		continue
	end
	% last two parts of file names mark the sampling rate and the total counts.
	% sampling_rate is computed from reading the distance between two
	% consequent rows' times cells, in the file. The total counts is just
	% `size(counts)`.
	[ counts, times ] = get_xy_data(fname);
	sampling_rate = 1/(times(2)-times(1));
	[f, p, cvg, iter, corp, covp, covr, stdresid, Z, r2] = fit_generic_curve(counts, times);
	measurements(fi - induced_index) = struct( ...
		'fname', fname, ...
		'times', times, ...
		'counts', counts, ...
		'magnet', magnet, ...
		'magnetOrientation', magnetOrientation, ...
		'samplingRate', sampling_rate, ...
		'totalCounts', size(counts, 1), ...
		'fitResults', struct( ...
			'successful', cvg, ...
			'r2', r2, ...
			% See https://en.wikibooks.org/wiki/Trigonometry/Simplifying_a_sin(x)_%2B_b_cos(x)
			'amplitude', sqrt(p(1)^2 + p(4)^2), ...
			'tau', 1/(2 * p(2)), ...
			'frequency', p(3), ...
			'computedValues', f ...
		)
	);
end

function plotM(fig_h, m)
	data_h = plot(m.times, m.counts, '+');
	hold on;
	fit_h = plot(m.times, m.fitResults.computedValues, '-');
	xlabel("Time");
	ylabel("offset of teeths");
	data_title = sprintf('Magnet: %s, Sampling Rate: %d, Total Counts: %d', ...
		m.magnetOrientation, ...
		m.samplingRate, ...
		m.totalCounts ...
	);
	fit_title = sprintf('Frequency: %f [hz], Tau: %f [s], Amplitude: %f [teeth], R^2: %f', ...
		m.fitResults.frequency, ...
		m.fitResults.tau, ...
		m.fitResults.amplitude, ...
		m.fitResults.r2 ...
	);
	legend([data_h fit_h], { ...
			data_title,
			fit_title
		}, ...
		'Location', "north" ...
	)
	legend('boxoff')
end

nomagnet_measurements = measurements([measurements.magnet] == 0);
for i=1:length(nomagnet_measurements)
	plotM(figure(), nomagnet_measurements(i))
	print(sprintf('nomagnets%d.png', i))
	print(sprintf('nomagnets%d.svg', i))
end

magnet_measurements = measurements([measurements.magnet] == 1);

for i=1:length(magnet_measurements)
	plotM(figure(), magnet_measurements(i))
	print(sprintf('magnet-%s-%d.png', magnet_measurements(i).magnetOrientation, i))
	print(sprintf('magnet-%s-%d.svg', magnet_measurements(i).magnetOrientation, i))
end
