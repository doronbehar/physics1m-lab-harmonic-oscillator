%! TEX program = xelatex
\documentclass[a4paper]{article}

\usepackage{amsmath}
\usepackage{siunitx}
% See https://tex.stackexchange.com/a/82462/125609
\usepackage{caption}
\captionsetup[figure]{labelformat=empty}% redefines the caption setup of the figures environment in the beamer class.
\usepackage{titling}

% Links
\usepackage{hyperref}
\hypersetup{colorlinks = true,
	citecolor = gray,
	linkcolor = red,
	citecolor = green,
	filecolor = magenta,
	urlcolor = cyan
}
% Drawings and figures
\usepackage[american,siunitx]{circuitikz}
\usepackage{graphicx}
\usepackage{listings}
\lstset{basicstyle=\footnotesize,
	tabsize=4,
	keywordstyle=\color{blue},
	stringstyle=\color{red},
	commentstyle=\color{green},
	morecomment=[l][\color{magenta}],
}

%% the following commands are needed for some matlab2tikz features
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usetikzlibrary{plotmarks}
\usetikzlibrary{arrows.meta}
\usepgfplotslibrary{patchplots}
\usepackage{grffile}
\usetikzlibrary{calc,patterns,angles,quotes}

% Looks
\usepackage[top=1in,bottom=1in,inner=3cm,outer=2cm]{geometry}
% Provides commands to disable pagebreaking within a given vertical space. If
% there is not enough space between the command and the bottom of the page, a
% new page will be started.
\usepackage{needspace}
% For tables
\usepackage{booktabs}
\usepackage{color}
\usepackage{mathtools}
% Remove section and chapters numbers from headings -
% https://tex.stackexchange.com/questions/297265/remove-sections-numbering
\setcounter{secnumdepth}{0}

% Language
\usepackage{polyglossia}
\setdefaultlanguage{hebrew}
\setotherlanguage{english}
\usepackage{hebrewcal}

% Fonts
\setmainfont{DejaVu Serif}
\setsansfont{DejaVu Sans}
\setmonofont{DejaVu Sans Mono}
\newfontfamily\hebrewfont{Frank Ruehl CLM}[Script=Hebrew]
\newfontfamily\hebrewfontsf{FreeSerif}[Script=Hebrew]
\newfontfamily\hebrewfonttt{Liberation Mono}[Script=Hebrew]

% Fix for a parentheses issue with hebrew fonts + polyglossia, see
% https://tex.stackexchange.com/a/141437/125609
\makeatletter
\def\maketag@@@#1{\hbox{\m@th\normalfont\LRE{#1}}}
\def\tagform@#1{\maketag@@@{(\ignorespaces#1\unskip)}}
\makeatother

\begin{document}

\title{
	מעבדת פיזיקה 1מ \\
	ניסוי אוסילטור הרמוני מרוסן
}
\author{\\
	דורון בכר \\
	ת.ז 316097872 \\
}

\begin{titlingpage}
\maketitle
\begin{abstract}

מדדנו את תנועתה של עגלה על משטח חסר חיכוך בתור מודל לאוסילטור הרמוני, מרוסן ולא
מרוסן. העגלה הייתה מחוברת לשני קפיצים משני צידיה ומדדנו את מיקום העגלה כתלות
בזמן.  השווינו את תדירות התנודות עם המודל התיאורטי לתנועת מתנד הרמוני, עם
התוצאות שלנו. קיבלנו שהמודל מנבא טוב מאוד את אופי התנועה, אבל הערכים שקיבלנו
לתדירות התנודות היו מעט רחוקים מהתאוריה.

\end{abstract} \end{titlingpage}

\section{מבוא}

\subsection{המודל התיאורטי}

בניסוי זה רצינו לבדוק את המודל הפיזיקלי שמקשר בין קבוע קפיץ, ומסה, לתדירות
התנודות של מתנד. תנועה כזו נקראת תנועה של מתנד הרמוני כי במודל, המהירות, התאוצה
והמיקום של המסה כולם מתקבלים כפונקציות מחזוריות בזמן.

כידוע, בפתרון הבעיה באופן תיאורטי, עבור מסה $m [kg]$, ושני קפיצים
שמחוברים בתור, בעלי הקבועים $k_1$ ו-$k_2$, מתקבל מפתרון משוואת כוחות
שמיקום המסה, $X(t) [m]$ כתלות בזמן היינה:

\begin{equation} \label{eq:asin}
X(t) = A e^{-\frac{t}{2\tau}} \cos({\omega_1 t + \phi_1})
\end{equation}

כאשר $A$, ו-$\phi_1$ נקבעים לפי תנאי ההתחלה (אצלנו $X(0) = x_{max}$
ו-$V(t)=\dot{X}(t) = 0$), $\tau$ הוא קבוע בעל מימדי זמן שמבטא את
מידת החיכוך שפרופורציוני למהירות וכן $ \omega_1 = \sqrt{\omega_0^2 -
\left ( \frac{1}{2 \tau} \right)^2} $ היא תדירות התנודות
ו- $\omega_0 = \sqrt{\frac{k_1 + k_2}{m}}$, שניהם ביחידות של $\frac{1}{[s]}$.

\subsection{אופן המדידה}

בנינו מתנד הרמוני, באמצעות עגלה שהורכבה על מסילה וכשהיא מחוברת לשני
קפיצים משני צידיה. כמתואר בתמונה: 

\begin{figure}[h]
	\includegraphics[width=0.6\textwidth]{schematic-fig.png}
\caption{}
\label{fig:schematic-illustration}
\end{figure}

המסילה בה השתמשנו כמעט חסרת חיכוך כי היא מוציאה אוויר מלמטה כדי לגרום לעגלה
לרחף (החיכוך היחיד שנשאר הוא עם האוויר). על העגלה ישנן שיניים צפופות מאוד
שבאמצעותן מדדנו את מהירותה עם גלאי פוטואלקטרי \textenglish{"Kruze"} שספר את
כמות השיניים לפיהם העגלה זזה, ימינה ושמאלה וכך למעשה הוא מדד את מיקומה בזמן,
בהתאם לקצב הדגימה שלו.

בשביל להוסיף למערכת חיכוך שיהיה פרופורציוני למהירות, השתמשנו במגנט שהונח על
העגלה. הנחנו אותו על העגלה פעם אחת בניצב ופעם אחת במקביל לכיוון התנועה. הרעיון
הוא שהמגנט ייצור זרמי מערבולת שמגמתם תמיד נגד כיוון המהירות, לפי חוק
\textenglish{Lenz}. לכן מתקבל כוח חיכוך שפרופורציוני למהירות, כמו
במודל התיאורטי. למגנט שהנחנו, מסה של $6.66 \pm 0.01 [g]$. 

כשלא היה מגנט, התייחסנו לחיכוך כקטן מאוד לכן ציפינו לקבל תדירות $\omega_1
\approx \omega_0 = \sqrt{\frac{k_1 + k_2}{m}}$. כשהיה קפיץ, ציפינו לראות
שהתנועה אכן מרוסנת אבל לא הייתה באפשרותנו להשוות את הריסון הנמדד עם הריסון
התאורטי כי אין ברשותנו דרך למדוד את עוצמת השדה המגנטי.

את קבועי הקפיץ מדדנו באמצעות תליית משקולות עם מסה ידועה לקפיץ מלמטה ואת קצהו
השני של הקפיץ למוט. את המסה, מחוברת לקפיץ הנחנו על מד משקל, וידענו מבעוד מועד
את המרחק בין האורך הרפוי של הקפיץ לאורך אליו הוא הגיע עד למטה בגלל המשקולת.
באמצעות השינוי באורך הקפיץ והשינוי במדד משקל המסה חישבנו את קבוע הקפיץ.

כשביצענו את המדידות, לא הקפדנו לשים לב לתנאי ההתחלה או אולי לאפס את הזמן של
גלאי המהירות, לכן לא יכולנו להתייחס בתוצאות לתנאי ההתחלה בהשוואה לתיאוריה.

סך הכל ביצענו 3 מדידות בלי שום ריסון חיצוני (לא הייתה באפשרותנו למנוע ריסון
כתוצאה מחיכוך עם האוויר) וביצענו 3 מדידות כאלו למשכי זמן שונים. בנוסף ביצענו 2
מדידות עם ריסון של מגנט בניצב לכיוון המהירות ועוד 2 מדידות עם מגנט בכיוון מקביל
למהירות, שוב, למשכי זמן שונים.

\clearpage

\section{תוצאות הניסוי}

בגרף זה אפשר לראות מדידה אחת שנעשתה בלי ריסון חיצוני, יחד עם ההתאמה
הלא לינארית למודל התאורטי (משוואה \ref{eq:asin}), לכל מדידה.

\begin{figure}[h]
\includegraphics[width=\textwidth]{nomagnets1.png}
\caption{}
\label{fig:nomagnets}
\end{figure}

\clearpage

בגרף זה רואים את תוצאות המדידה וההתאמה למודל כששמנו את המגנט במאונך לכיוון התנועה:

\begin{figure}[h]
\includegraphics[width=\textwidth]{magnet-perpendicular-3.png}
\caption{}
\label{fig:magnet-penpendicular}
\end{figure}

\clearpage

וכאן המגנט היה מקביל לכיוון התנועה:

\begin{figure}[h]
\includegraphics[width=\textwidth]{magnet-horiz-1.png}
\caption{}
\label{fig:magnet-horizontal}
\end{figure}

\href{https://gitlab.com/doronbehar/physics1m-lab-harmonic-oscillator}{בתיקיית
הניסוי}, ישנן עוד תמונות של כל הגרפים שהתקבלו מכל המדידות.

\clearpage

\section{דיון בתוצאות}

\subsection{השוואה עם התיאוריה}

בכל המדידות וגם באילו שלא מוצגות כאן, קיבלנו התאמה מדויקת מאוד גם
לפי העין וגם לפי הערך של $R^2$. $\tau$ היה גדול כשלא היה ריסון, והיה
יותר קטן כשהיה ריסון. הריסון היה חזק אף יותר במדידה המוצגת בגרף השני
(\ref{fig:magnet-penpendicular}), שוב כמצופה מהמודל התאורטי של חוק
\textenglish{Lenz} - כי השדה המגנטי היה ניצב לכיוון התנועה ולא מקביל אליה.

% SPRING1_K = (sym) 1.7643535589586522480537133472074
% SPRING2_K = (sym) 1.1722416967840733556038389810565
% FREQENCY0_NO_MAGNET = (sym) 0.59629523369265465468752896107014
% FREQENCY0_WITH_MAGNET = (sym) 0.58702431670230131894242564924401
% dk(delta_x, delta_mg, x, dmg) = (symfun)
% 
%         __________________
%        ╱     2     2    2 
%       ╱  δ_mg    δₓ ⋅dmg  
%      ╱   ───── + ──────── 
%     ╱       2        4    
%   ╲╱       x        x     
% 
% DELTA_K1 = (sym) 0.0019970045941807042726426376163945
% DELTA_K2 = (sym) 0.0019970045941807042726426376163945

לפי המודל התאורטי, עבור המתנד הלא מרוסן, אמור להתקבל:

$$f = \frac{\omega_1}{2\pi} \approx \frac{\omega_0}{2\pi} = \sqrt{\frac{k_1 + k_2}{m}} \frac{1}{2\pi} = 1.188 [1/s]$$

כאשר:

$$ k_1 = 0.176 \pm 0.001, k_1 = 1.117 \pm 0.001
[N/m]$$\footnote{השגיאה הנגררת חושבה באמצעות התוכנה, כפי שאפשר לראות
\href{https://gitlab.com/doronbehar/physics1m-lab-harmonic-oscillator/blob/a6893ae201111467a17506a7c9bd171dfe91e0f0/harmonic-oscillator.m\#L12-36}{בקובץ}.}

יש פער בין התדירות הנמדדת למצופה: $0.102 [1/s]$ לעומת $0.143 [1/s]$
וזה בסדר גודל של $10\%$. סיבה אפשרית לכך היא שהמדידה שלנו את קבועי
הקפיץ לא הייתה מוצלחת. הקפיצים שלנו אולי לא אידאלים - כלומר יכול
להיות שההתנגדות שלהם לא לינארית לשינוי באורכם. ראוי לציין שהתדר
שהתקבל למתנדים המרוסנים היה קרוב יותר ל-$\frac{\omega_0}{2\pi}$ אבל
דווקא עבור מתנד מרוסן, $\omega_0 \neq \omega_1$ לכן זה לא מצביע על
ניבוי טוב של המודל התאורטי.

חוץ מהתבוננות בגרפים עצמם, ניתן לראות שהערך של $\tau$ שנמדד לכל סט
דגימות, היה גדול יותר כשלא היה ריסון (\ref{fig:nomagnets}), קטן יותר
כשהיה מגנט במקביל לכיוון התנועה (\ref{fig:magnet-horizontal}), ועוד
יותר קטן כשהמגנט היה במאונך לכיוון התנועה
(\ref{fig:magnet-penpendicular}). בנוסף, ראוי לציין שהתדירות שנמדדה
הייתה זהה בכל מצב של ריסון, מה שמראה שהמודל ניבא טוב את העובדה
שהריסון לא משפיעה על התדירות. 

\subsection{שגיאות המדידה}

מכשיר המדידה היחיד שלקח חלק במדידות היה החיישן הפוטואלקטרי. הוא ספר כמה שיניים
ימינה ושמאלה העגלה עברה, בתדר דגימה מסוים. תדר הדגימה עבור המדידה הראשונה
(\ref{fig:nomagnets}), הינו 100 מדידות לשנייה. לכן השגיאה היא $\pm0.001[s]$. על
הגרף לא ניתן היה להציג רווח בר סמך כי גודלו היה קטן מידי.

\section{מסקנות}

המודל התאורטי ניבא טוב מאוד את אופי התנועה - קיבלנו תנועה מחזורית עם
התאמה מעולה לגל מחזורי בצורת סינוס. אף על פי כן, ניבוי התדר של המודל
פחות מוצלח לפי הניסוי הזה - התדירויות יצאו די רחוקות מהמצופה. ההשערה
שלנו היא שזה בגלל שהקפיצים שקיבלנו לא אידאלים ועבורם חוק הוק אינו
מדויק לגמרי. כמו כן, נראה שהמודל מתאר טוב גם תנועה מרוסנת כי ההתאמה
לגרף הייתה טובה גם כן.

בנוסף, בניסוי זה בדקנו גם בעקיפין את חוק \textenglish{Lenz} כשהעמדנו
את המגנט בניצב ובמקביל לתנועה. גם את מודל זה הצלחנו לאשש בצורה די
מוצלחת כי ניכר היה שהדעיכה גדולה יותר כאשר הכוחות המגנטיים גורמים
לזרמי מערבולת חזקים יותר כשהם בכיוון המתאים, לפי כלל יד ימין.

\section{נספח}

הקוד שנכתב לצורך עיבוד הנתונים, חישוב השגיאות, והנתונים עצמם נמצאים
\href{https://gitlab.com/doronbehar/physics1m-lab-harmonic-oscillator}{כאן}.

\end{document}
