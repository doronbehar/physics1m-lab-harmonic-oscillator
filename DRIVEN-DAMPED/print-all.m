% Plot steady measurements
for mi=1:length(steady_measurements)
	figure();
	plotM(steady_measurements(mi));
	print(sprintf("%s.svg", steady_measurements(mi).fname))
	print(sprintf("%s.png", steady_measurements(mi).fname))
end

% Plot tau measurements
for mi=1:length(tau_measurements)
	figure();
	plotM(tau_measurements(mi));
	print(sprintf("%s.svg", tau_measurements(mi).fname))
	print(sprintf("%s.png", tau_measurements(mi).fname))
end
