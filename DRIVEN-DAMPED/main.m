#!/usr/bin/env octave

% Load package that fits non linear curves to data points
pkg load optim
% to findpeaks
pkg load signal

% Function to print stuff nicely
function print_measurements(m, name)
	disp("")
	disp(sprintf("@@@@@@@@@    %s   @@@@@@@@@", name))
	disp("")
	%% Define functions for later use in computing errors
	% constant
	EARTH_G = sym(9.78033, 'f');
	syms k(dm,x, g);
	k(dm,x, g) = (dm * g)/x;
	syms omega0(k1, k2, m1, m2);
	omega0(k1, k2, m1, m2) = sqrt((k1 + k2)/(m1 + m2));
	% To compute the error
	syms dk(delta_x, delta_m, x, dm, g);
	dk(delta_x, delta_m, x, dm, g) = sqrt((diff(k, x) * delta_x)^2 + (delta_m * diff(k, dm))^2);
	SPRING1_K = vpa(k(m.spring1_dmass, m.dx, EARTH_G))
	SPRING2_K = vpa(k(m.spring2_dmass, m.dx, EARTH_G))
	OMEGA0_NO_MAGNET = vpa(omega0(SPRING1_K, SPRING2_K, 0, m.wagon_mass))
	OMEGA0_WITH_MAGNET = vpa(omega0(SPRING1_K, SPRING2_K, m.magnets_mass, m.wagon_mass))
	syms domega(delta_k, delta_m, k1, k2, m1, m2);
	domega(delta_k, delta_m, k1, k2, m1, m2) = sqrt( ...
		(diff(omega0, k1)*delta_k)^2 + ...
		(diff(omega0, k2)*delta_k)^2 + ...
		(diff(omega0, m1)*delta_m)^2 + ...
		(diff(omega0, m2)*delta_m)^2 ...
	);
	DELTA_M = sym(0.1*0.001, 'f');
	% We assume the length of the 97.95 is totally correct, because it came
	% that way from the supplier
	DELTA_X = sym(0, 'f')
	DELTA_K1 = vpa(dk(DELTA_X, DELTA_M, m.dx, m.spring1_dmass, EARTH_G))
	DELTA_K2 = vpa(dk(DELTA_X, DELTA_M, m.dx, m.spring2_dmass, EARTH_G))
	DELTA_OMEGA0 = domega(DELTA_K2, DELTA_M, SPRING1_K, SPRING2_K, sym(0, 'f'), m.wagon_mass)
end
m = struct();
% NOTE: m.dx should be the same for both DORON and YAHAV
m.dx = sym(97.95*0.001, 'f');
%% DORON MEASUREMENTS
m.spring1_dmass = sym(60.6*0.001, 'f');
m.spring2_dmass = sym(42.2*0.001, 'f');
m.wagon_mass = sym(207.4*0.001, 'f');
m.magnets_mass = sym(6.7*0.001, 'f');
print_measurements(m, "DORON")
%% YAHAV MEASUREMENTS
m.spring1_dmass = sym(33.6*0.001, 'f');
m.spring2_dmass = sym(66.2*0.001, 'f');
m.wagon_mass = sym(209.1*0.001, 'f');
% TODO: Ask what that was
m.magnets_mass = sym(6.8*0.001, 'f');
print_measurements(m, "YAHAV")

%
% Get data out of a file
%
function [counts, times] = get_xy_data(fname, channel)
	file = fopen(fname);
	% All measurements txt files have the same format
	data = textscan(file, '%d %f %f %f', 'HeaderLines', 1);
	fclose(file);
	% The 1st column is not interesting, it's just an index
	times = data{2};
	if strcmp(channel, "A")
		counts = data{3};
	elseif strcmp(channel, "B")
		counts = data{4};
	else
		error("unknown channel specified")
	end
end

global steady_param_func
steady_param_func = @(t, p) p(1)*sin(2*pi*p(2)*t) + p(3)*cos(2*pi*p(2)*t) + p(4);
function [f, p, cvg, iter, corp, covp, covr, stdresid, Z, r2, peaks, peaks_i] = fit_steady_curve(counts, times)
	% Compute the initial values - according to the point where the speed was
	% minimal: When the right most column (count column) was at it's lowest
	% value, it's where we should have a zero speed
	[min_c, min_i] = min(counts);
	[max_c, max_i] = max(counts);
	% We replace negative values with zeros in counts before passing them to
	% findpeaks, see:
	% https://www.mathworks.com/matlabcentral/answers/377607-how-to-replace-negative-elements-in-a-matrix-with-zeros
	[peaks, peaks_i] = findpeaks(max(counts, 0), 'MinPeakDistance', abs(max_i-min_i)/4);
	% The real amplitude will eventually be sqrt(p(1)^2 + p(4)^2), so this is a
	% good guess for both the sin and cos coefficients.
	max_amplitude = sqrt(mean(peaks));
	% The frequency is given roughly by 1 over 2*( the distance between the 2
	% first min - max )
	frequency = 1/(mean(diff(times(peaks_i))));
	guessed_values_arr = [ ...
		max_amplitude; ...
		frequency; ...
		max_amplitude; ...
		% Offset
		0; ...
	];
	% Bring it to scope
	global steady_param_func
	[f, p, cvg, iter, corp, covp, covr, stdresid, Z, r2] = leasqr( ...
		times, ...
		counts, ...
		guessed_values_arr, ...
		steady_param_func ...
	);
end

global tau_param_func
tau_param_func = @(t, p) exp(-t * p(2)) .* (p(1)*sin(2*pi*p(3)*t) + p(4)*cos(2*pi*p(3)*t)) + p(5);
function [f, p, cvg, iter, corp, covp, covr, stdresid, Z, r2, peaks, peaks_i] = fit_tau_curve(counts, times)
	% Compute the initial values - according to the point where the speed was
	% minimal: When the right most column (count column) was at it's lowest
	% value, it's where we should have a zero speed
	[min_c, min_i] = min(counts);
	[max_c, max_i] = max(counts);
	% the maximum value is half of that of the real value because in parts
	% of the plot, the speed is "negative"
	max_amplitude = max_c/2;
	% The frequency is given roughly by 1 over 2*( the distance between the 2
	% first min - max )
	frequency = 1/(2 * abs(times(max_i) - times(min_i)));
	% We replace negative values with zeros in counts before passing them to
	% findpeaks, see:
	% https://www.mathworks.com/matlabcentral/answers/377607-how-to-replace-negative-elements-in-a-matrix-with-zeros
	[peaks, peaks_i] = findpeaks(max(counts, 0), 'MinPeakDistance', abs(max_i-min_i)/4);
	% We compute an initial guess for tau, based on the fact that as t grows, the
	% ratio between the every local maxima, is somewhat proportional to it
	dt = times(peaks_i(end) - peaks_i(1));
	k = peaks(1)/peaks(end);
	tau = dt/k;
	inversed_tau = 1/(2 *tau);
	% Fit the graph
	guessed_values_arr = [ ...
		max_amplitude; ...
		inversed_tau; ...
		frequency; ...
		% max_amplitude is a good estimation both for sin and cos coefficients
		max_amplitude; ...
		0;
	];
	% Bring it to scope
	global tau_param_func
	[f, p, cvg, iter, corp, covp, covr, stdresid, Z, r2] = leasqr( ...
		times, ...
		counts, ...
		guessed_values_arr, ...
		tau_param_func, ...
		1e-9 ...
	);
end

steady_mfiles = dir("measurements/steady-*.txt");
global steady_measurements
steady_measurements = struct();
for fi=1:length(steady_mfiles)
	fname = sprintf("measurements/%s", steady_mfiles(fi).name)
	measurement_type = "steady";
	% The Mass
	mass = struct();
	[mass.counts, mass.times] = get_xy_data(fname, "B");
	[mass.f, mass.p, mass.cvg, mass.iter, mass.corp, mass.covp, mass.covr, mass.stdresid, mass.Z, mass.r2] = fit_steady_curve(mass.counts, mass.times);
	% The Driving force
	drive = struct();
	[drive.counts, drive.times] = get_xy_data(fname, "A");
	[drive.f, drive.p, drive.cvg, drive.iter, drive.corp, drive.covp, drive.covr, drive.stdresid, drive.Z, drive.r2] = fit_steady_curve(drive.counts, drive.times);
	% Given both fit results, compute shared measurements results
	drive_amplitude = sqrt(drive.p(1)^2 + drive.p(3)^2);
	mass_amplitude = sqrt(mass.p(1)^2 + mass.p(3)^2);
	amplitude_factor = mass_amplitude/drive_amplitude;
	freq = mean([drive.p(2) mass.p(2)]);
	% Compute phase
	phase = phase_diff(drive.f, mass.f)
	steady_measurements(fi) = struct( ...
		'fname', fname, ...
		'type', "steady", ...
		% Should be the same for both mass and drive
		'sampling_rate', 1/(mass.times(2) - mass.times(1)), ...
		'mass', struct( ...
			'times', mass.times, ...
			'counts', mass.counts ...
		),
		'drive', struct( ...
			'times', drive.times, ...
			'counts', drive.counts ...
		),
		% Should be the same for both mass and drive
		'total_counts', size(mass.counts, 1), ...
		'fit', struct( ...
			'r2', mean([mass.r2, drive.r2]), ...
			'mass_p', mass.p, ...
			'drive_p', drive.p, ...
			'amplitude_factor', amplitude_factor, ...
			'successful', drive.cvg + mass.cvg, ...
			'frequency', freq, ...
			'phase', phase, ...
			'computed_values', struct( ...
				'mass', mass.f,
				'drive', drive.f
			)
		)
	);
end

tau_mfiles = dir("measurements/tau-*.txt");
global tau_measurements
tau_measurements = struct();
for fi=1:length(tau_mfiles)
	fname = sprintf("measurements/%s", tau_mfiles(fi).name)
	[counts, times] = get_xy_data(fname, "B");
	sampling_rate = 1/(times(2)-times(1));
	[f, p, cvg, iter, corp, covp, covr, stdresid, Z, r2] = fit_tau_curve(counts, times);
	tau_measurements(fi) = struct( ...
		'fname', fname, ...
		'times', times, ...
		'type', "tau", ...
		'counts', counts, ...
		'sampling_rate', sampling_rate, ...
		'total_counts', size(counts, 1), ...
		'fit', struct( ...
			'successful', cvg, ...
			'r2', r2, ...
			% See https://en.wikibooks.org/wiki/Trigonometry/Simplifying_a_sin(x)_%2B_b_cos(x)
			'amplitude', sqrt(p(1)^2 + p(4)^2), ...
			'tau', 1/(2 * p(2)), ...
			'frequency', p(3), ...
			'computed_values', f ...
		)
	);
end

run plot-lib.m

run amp-omega.m

run tau.m

run yahav-phase-omega.m
