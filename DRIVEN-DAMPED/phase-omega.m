handmade_phases = [handmade_fits.phase];
computed_phases = [[steady_measurements.fit].phase];
all_phases = cat(2, handmade_phases, computed_phases);

hold on;
sorted_phases = all_phases(all_freqs_i);
plot(all_freqs, sorted_phases, 'o')
adjusted_phases = sorted_phases;
% Sorted according to frequencies
for p_i=1:length(adjusted_phases)
	if adjusted_phases(p_i) > pi/2
		adjusted_phases(p_i) = adjusted_phases(p_i) - pi;
	end
end
plot(all_freqs, adjusted_phases, '+')
