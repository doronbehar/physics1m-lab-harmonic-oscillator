% Hand made
phase_diff = [ -0.3679   -0.2287   -0.1740   -0.1261   -0.1203   -0.0791   -0.0571   -0.0071   -0.4668    0.4523 0.1790    0.1367    0.1154    0.0628   -0.2412   -1.4904];
freqs = [ 7.0950    7.4490    7.7580    8.2030    8.5770    9.5220   10.0900   12.8700    6.9800    6.5210 5.7250    5.0940    4.3430    3.1570    7.3010    6.7410 ];
figure();
plot(freqs, phase_diff, 'o');
hold on;
PHASE_FIT_R2 = 0.9882;
tau = 3.909;
% By the cftool
tau_min = 3.398;
tau_max = 4.421;
tau_pm = mean([abs(tau-tau_max), abs(tau-tau_min)])
omega0 = 6.733;
omega0_min = 6.721;
omega0_max = 6.746;
omega0_pm = mean([abs(omega0-omega0_max), abs(omega0-omega0_min)])
handmade_phase_fit = @(omega) atan(omega./(tau.*(omega0.^2 - omega.^2)));

x = linspace(2, 13, 8000);
plot(x, handmade_phase_fit(x))
xlabel("\\omega [rad/s]");
ylabel("\\phi [rad]");
legend({
	'Measurements',
	'Fit'
}, 'Location', "northwest")

matlab2tikz( ...
	'filename', "phase-plot.tex", ...
	% To make the file lighter
	'externalData', true, ...
	'extraCodeAtEnd', sprintf( ...
		% the points in %f were picked by hand, according to standards of writing errors
		"\\caption{כאן התקבלה התאמה עם $ R^2 = %0.3f $, $\\tau = %0.1f\\pm%0.1f [s]$ ו-$\\omega_0 = %0.2f\\pm%0.2f [rad/s]$}", ...
		PHASE_FIT_R2, tau, tau_pm, omega0, omega0_pm
	), ...
	'showInfo', false ...
);
