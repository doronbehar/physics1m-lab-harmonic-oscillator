%% Start fitting Amplitude vs omega

handmade_fits = [ ...
	struct(...
		'mass_amp', 77.04, 'drive_amp', 10.52,
		'phase', 0.489,
		'omega', 6.707
	),
	struct(...
		'mass_amp', 7.924, 'drive_amp', 10.39,
		'phase', 2.5588,
		'omega', mean([8.559, 8.561])
	),
	struct(...
		'mass_amp', 5.326, 'drive_amp', 10.53,
		'phase', 2.580,
		'omega', 9.28
	),
	struct(...
		'mass_amp', 12.04, 'drive_amp', 29.19,
		'phase', 2.6411,
		'omega', 9.854
	),
	struct(...
		'mass_amp', 12.64, 'drive_amp', 54.41,
		'phase', 2.593,
		'omega', 11.79
	),
	struct(...
		'mass_amp', 9.165, 'drive_amp', 54.56,
		'phase', 2.537,
		'omega', 13.3
	),
	struct(...
		'mass_amp', 99.87, 'drive_amp', 10.2,
		'phase', 0.841,
		'omega', 6.792
	),
];

computed_amp_factors = [[steady_measurements.fit].amplitude_factor];
computed_freqs = [[steady_measurements.fit].frequency];
handmade_amp_factors = [handmade_fits.mass_amp]./[handmade_fits.drive_amp];
handmade_freqs = [handmade_fits.omega]/(2*pi);

[all_freqs, all_freqs_i] = sort(cat(2, handmade_freqs, computed_freqs));
all_amp_factors = cat(2, handmade_amp_factors, computed_amp_factors)(all_freqs_i);

figure()
plot(all_freqs*2*pi, all_amp_factors, 'o')

% Not used, fitting was done by cftool at Yahav's and the parameters are written here
% amp_omega_param_func = @(t, p) p(1)/sqrt((p(2)-p(3))^2+(p(3)*p(4))^2);
omega0 = 6.856;
omega0_min = 6.855;
omega0_max = 6.857;
omega0_pm = mean([abs(omega0-omega0_max), abs(omega0-omega0_min)])
AMPOMEGA_FIT_R2 = 1;
tau = 3.668;
tau_min = 3.64;
tau_max = 3.697;
tau_pm = mean([abs(tau-tau_max), abs(tau-tau_min)])
F0 = 4.155;
F0_min = 4.138;
F0_max = 4.172;
% Hand picked from symbolic measurements at main.m
mass = 207.4*0.001;
hand_made_fit_func = @(omega) (F0./mass)./sqrt((omega0.^2 - omega.^2).^2 + (omega./tau).^2);
hold on;
x = linspace(0, 13, 10000);
plot(x, hand_made_fit_func(x))
xlabel("\\omega [rad/s]");
ylabel("Amplitude Factor");
legend({
	'Measurements',
	'Fit'
}, 'Location', "northwest")

MEAN_R2 = mean([[steady_measurements.fit].r2])
MIN_R2 = 0.9793 % By Yahav's hand made fits
matlab2tikz( ...
	'filename', "amp-omega-plot.tex", ...
	% To make the file lighter
	'externalData', true, ...
	'extraCodeAtEnd', sprintf( ...
		% the points in %f were picked by hand, according to standards of writing errors
		"\\caption{כאן התקבלה התאמה עם $ R^2 = %0.3f $, $\\tau = %0.2f\\pm %0.2f [s]$ ו-$\\omega_0 = %0.3f\\pm%0.3f [rad/s]$}", ...
		AMPOMEGA_FIT_R2, tau, tau_pm, omega0, omega0_pm
	), ...
	'showInfo', false ...
);

% this values were retrieved using Wolfram alpha's Solve. Now using our hand
% made fit values, we calculate the theoritcal DELTA_OMEGA, to compute Q. 
DELTA_OMEGA = sqrt( ...
	-1/(2*tau^2) + omega0^2 + sqrt(1 + 4 * tau^2 * omega0^2)/(2*tau^2)
) - sqrt( ...
	-1/(2*tau^2) + omega0^2 - sqrt(1 + 4 * tau^2 * omega0^2)/(2*tau^2)
)

Q = omega0/DELTA_OMEGA
