%! TEX program = xelatex
\documentclass[a4paper]{article}

\usepackage{amsmath}
\usepackage{siunitx}
% See https://tex.stackexchange.com/a/82462/125609
\usepackage{caption}
\captionsetup[figure]{labelformat=empty}% redefines the caption setup of the figures environment in the beamer class.
\usepackage{titling}

% Links
\usepackage{hyperref}
\hypersetup{colorlinks = true,
	citecolor = gray,
	linkcolor = red,
	citecolor = green,
	filecolor = magenta,
	urlcolor = cyan
}
% Drawings and figures
\usepackage[american,siunitx]{circuitikz}
\usepackage{graphicx}
\usepackage{listings}
\lstset{basicstyle=\footnotesize,
	tabsize=4,
	keywordstyle=\color{blue},
	stringstyle=\color{red},
	commentstyle=\color{green},
	morecomment=[l][\color{magenta}],
}

%% the following commands are needed for some matlab2tikz features
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usetikzlibrary{plotmarks}
\usetikzlibrary{arrows.meta}
\usepgfplotslibrary{patchplots}
\usepackage{grffile}
\usetikzlibrary{calc,patterns,angles,quotes}

% Looks
\usepackage[top=1in,bottom=1in,inner=3cm,outer=2cm]{geometry}
% Provides commands to disable pagebreaking within a given vertical space. If
% there is not enough space between the command and the bottom of the page, a
% new page will be started.
\usepackage{needspace}
% For tables
\usepackage{booktabs}
\usepackage{color}
\usepackage{mathtools}
% Remove section and chapters numbers from headings -
% https://tex.stackexchange.com/questions/297265/remove-sections-numbering
\setcounter{secnumdepth}{0}

% Language
\usepackage{polyglossia}
\setdefaultlanguage{hebrew}
\setotherlanguage{english}
\usepackage{hebrewcal}

% Fonts
\setmainfont{DejaVu Serif}
\setsansfont{DejaVu Sans}
\setmonofont{DejaVu Sans Mono}
\newfontfamily\hebrewfont{Frank Ruehl CLM}[Script=Hebrew]
\newfontfamily\hebrewfontsf{FreeSerif}[Script=Hebrew]
\newfontfamily\hebrewfonttt{Liberation Mono}[Script=Hebrew]

% Fix for a parentheses issue with hebrew fonts + polyglossia, see
% https://tex.stackexchange.com/a/141437/125609
\makeatletter
\def\maketag@@@#1{\hbox{\m@th\normalfont\LRE{#1}}}
\def\tagform@#1{\maketag@@@{(\ignorespaces#1\unskip)}}
\makeatother

\begin{document}

\title{
	מעבדת פיזיקה 1מ \\
	התלות בין תדירות מתנד מאלץ לתנועה הרמונית מרוסנת מאולצת
}
\author{\\
	דורון בכר \\
	ת.ז 316097872 \\
	יהב אורעד \\
	ת.ז 206223810 \\
}

\begin{titlingpage}
\maketitle
\begin{abstract}

מדדנו את תנועתה של עגלה על משטח חסר חיכוך בתור מודל לאוסילטור הרמוני, מרוסן
ומאולץ. העגלה הייתה מחוברת לשני קפיצים ולמנוע, כמאלץ. מדדנו את מיקום העגלה
כתלות בזמן ומדדנו את תדירות התנודות שלה ואת הפרש הפאזה בין המיקום שלה למיקום
המנוע. השווינו את תוצאה זו עם התוצאה מהמודל התיאורטי לתנועת מתנד הרמוני מאולץ
ומרוסן וקיבלנו משרעת מקסימלית וזווית שקרובה ל-$\pi/2$, בסמיכות לתדר התהודה.

\end{abstract} \end{titlingpage}

\section{מבוא}

\subsection{המודל התיאורטי}

בהינתן מסה $m [kg]$, שמחוברת לקפיצים עם קבועים $k_1 [N/m]$ ו-$k_2 [N/m]$ מצד
אחד, ולאובייקט אחר שמפעיל כוח מהצורה:

\begin{equation} \label{eq:force-form}
	F(t) = F_0 \cos(\omega t)
\end{equation}

כאשר $\omega [1/s]$ היא תדירות התנודות של האובייקט וכאשר יש חיכוך שתלוי במהירות
המסה, מתקבל שמיקום המסה כתלות בזמן, במצב היציב הוא:

\begin{equation} \label{eq:mass(t)}
	X(t) = A_{max} \cos(\omega t + \phi)
\end{equation}

כאשר $\omega_0 = \sqrt{\frac{m}{k_1 + k_2}}$, הפרש הפאזה בין הכוח למסה הוא:

\begin{equation} \label{eq:phase}
\phi(\omega) = \arctan(-\frac{\omega}{\tau (\omega_0^2 - \omega^2)})
\end{equation}

והמשרעת המקסימלית של המסה היא:

\begin{equation} \label{eq:max_amplitude}
	A_{max}(\omega) = \frac{F_0}{m \sqrt{(\omega_0^2-\omega^2)^2 + (\frac{\omega}{\tau})^2}}
\end{equation}

$\tau [s]$, הוא מקדם שנקבע לפי מידת החיכוך במערכת. ערכו של $\tau$ הוא הזמן
שלוקח למערכת לדעוך ממשרעת מקסימלית, ל-$\frac{1}{e}$ ממנה, אם יש למסה אנרגיה ולא
מפעילים עליה כוח מאלץ. בגבול $\omega \rightarrow \omega_0$, מתקבל ש: $\phi
\rightarrow \frac{\pi}{2}$ ומשרעת המסה מקסימלית בביטוי \ref{eq:max_amplitude}.

מקדם האיכות של המערכת מוגדר בשתי דרכים שונות:

\begin{equation} \label{eq:Q-tau}
	Q = \omega_0 \tau
\end{equation}

או כך:

\begin{equation} \label{eq:Q-delta-omega}
	Q = \frac{\omega_0}{\Delta \omega}
\end{equation}

כאשר ${\Delta \omega}$ הוא הרוחב בין התדירויות בהן המשרעת מביטוי \ref{eq:max_amplitude} היא $\frac{1}{\sqrt{2}}$ מערכה המקסימלי, כאשר $\omega = \omega_0$.

\clearpage

\subsection{אופן המדידה}

בנינו מתנד הרמוני, באמצעות עגלה שהורכבה על מסילה וכשהיא מחוברת לשני קפיצים משני
צידיה, ובאחד הצדדים הרכבנו עגלה נוספת שפעלה כמתנד, והיא הייתה האובייקט המאלץ,
כמתואר בתמונה:

\begin{figure}[h]
	\includegraphics[width=0.7\textwidth]{../DRIVEN/schematic-fig.png}
\caption{מערך הניסוי}
\label{fig:schematic-illustration}
\end{figure}

העגלה המאלצת זזה בתדירות של המנוע. שהיא $\omega$ במודל התאורטי. המסילה בה
השתמשנו כמעט חסרת חיכוך כי היא מוציאה אוויר מלמטה כדי לגרום לעגלה לרחף. על
העגלה הרכבנו שני מגנטים, במאונך לכיוון התנועה, זאת בכדי להוסיף חיכוך למערכת
שיהיה פרופורציוני למהירות, לפי חוק \textenglish{Lenz}. על העגלה ישנן שיניים
צפופות מאוד שבאמצעותן מדדנו את מיקומה של העגלה עם גלאי פוטואלקטרי
\textenglish{"Kruze"} שספר את כמות השיניים לפיהם העגלה זזה ימינה ושמאלה. כך
מדדנו את מיקומה של העגלה בזמן.

את קבועי הקפיץ מדדנו באמצעות תליית משקולות עם מסה ידועה לקפיץ מלמטה ואת קצהו
השני של הקפיץ למוט. את המסה, מחוברת לקפיץ, הנחנו על מד משקל, וידענו מבעוד מועד
את המרחק בין האורך הרפוי של הקפיץ לאורך אליו הוא הגיע עד למטה בגלל המשקולת.
באמצעות השינוי באורך הקפיץ והשינוי במדד משקל המסה, חישבנו את קבוע הקפיץ.

למד המשקל יש שגיאה של $\pm 0.1 [g]$, ומחוק הוק לחישוב קבוע קפיץ,
שאומר $ k = \frac{\Delta m g}{\Delta x} $, התקבל ש: $k_1 = 3.35 \pm 0.01$
ו-$k_2 = 6.61 \pm 0.01$.\footnote{חישוב השגיאה התבצע
\href{https://gitlab.com/doronbehar/physics1m-lab-harmonic-oscillator/blob/master/DRIVEN-DAMPED/main.m\#L15-28}{כאן}.}

הפעלנו את המנוע של העגלה המאלצת במגוון תדירויות, וחיכינו שהתנועה תתייצב. עבור
כל תדירות במצב היציב, מדדנו אותה ורשמנו את היחס בין המשרעת המקסימלית של המסה
לבין המשרעת המקסימלית של העגלה המאלצת.

\clearpage

\section{תוצאות הניסוי}

את הניסוי ביצענו על 2 מערכות זהות שנבדלות במקצת במסות ובחיכוך של כל אחת. הנתונים שהוצגו במבוא, כמו קבועי הקפיץ, הם של מערכת א'. בזמן עיבוד הנתונים, ראינו שדווקא מערכת ב' מציגה יותר טוב את הקשר בין הפאזה לתדירות. לכן בחרנו להציג את הנתונים של מדידות הפאזה והתדירות בגרף \ref{fig:phase-omega} ממערכת ב'. את הנתונים שהצגנו בשני הגרפים הבאים, לקחנו מהמדידות של מערכת א'. בדיון כמובן התחשבנו בנתונים השונים בהשוואה לתאוריה.

בגרף זה אפשר לראות את מיקום המסה כתלות בזמן, כאשר לא הפעלנו כוח מאלץ והתנועה
התחילה עם אנרגיה התחלתית כלשהי. 

\begin{figure}[h]
	\input{tau-plot.tex}
	\label{fig:tau}
\end{figure}

הגרף שהותאם כאן הוא למשוואה ולתיאוריה שמוצגת בניסוי קודם.\footnote{דוח הניסוי
הקודם נמצא
\href{https://gitlab.com/doronbehar/physics1m-lab-harmonic-oscillator/-/blob/master/DAMPED/harmonic-oscillator.pdf}{כאן}.}
חוץ מערכו של $R^2$ מהתאמה זו, אין לנו נתונים אובייקטיבים על טיב ההתאמה של עבור
ערכו של $\tau$ שהשתמשנו בו בהמשך בדיון בתוצאות.
 
\clearpage

בגרף זה אפשר לראות את היחס בין משרעת העגלה למשרעת המסה, כתלות בתדירותן של המסה
והמאלץ. גרף ההתאמה הינו למשוואה \ref{eq:max_amplitude}.

\begin{figure}[h]
	\input{amp-omega-plot.tex}
	\label{fig:amp-omega}
\end{figure}

\clearpage

בגרף זה רואים את ההפרש הפאזה בין המסה לעגלה המאלצת, כתלות בתדירות, יחד עם
ההתאמה למשוואה \ref{eq:phase}.

\begin{figure}[h]
	\input{phase-plot.tex}
	\label{fig:phase-omega}
\end{figure}

כאמור, הנתונים בגרף זה הם של מערכת ב'.

\clearpage

\section{דיון בתוצאות}

% @@@@@@@@@    DORON   @@@@@@@@@
% 
% SPRING1_K = (sym) 6.0509239203675335597620398464884
% SPRING2_K = (sym) 4.2136796937212858102626884953741
% OMEGA0_NO_MAGNET = (sym) 7.0350423384924958056197863631567
% OMEGA0_WITH_MAGNET = (sym) 6.9240908545470123315218707932430
% DELTA_X = (sym) 0
% DELTA_K1 = (sym) 0.0099850229709035210249074506641205
% DELTA_K2 = (sym) 0.0099850229709035210249074506641205
% DELTA_OMEGA0 = (sym) 0.0054008453599129381043507042109609
% 
% @@@@@@@@@    YAHAV   @@@@@@@@@
% 
% SPRING1_K = (sym) 3.3549677182235833891424113442824
% SPRING2_K = (sym) 6.6100852067381314841359253022962
% OMEGA0_NO_MAGNET = (sym) 6.9033960290162392448494622537773
% OMEGA0_WITH_MAGNET = (sym) 6.7953852727702858593433947952979
% DELTA_X = (sym) 0
% DELTA_K1 = (sym) 0.0099850229709035210249074506641205
% DELTA_K2 = (sym) 0.0099850229709035210249074506641205
% DELTA_OMEGA0 = (sym) 0.0054197717711199576400720120438181

\subsection{השוואה עם התיאוריה}

מכל המדידות שביצענו זיקקנו 2 גרפים, של אמפליטודה ושל פאזה כנגד תדירות. מכל
המדידות, ההתאמות לגרפים הניבו ערכי $\tau$ ו-$\omega_0$.  הערך של $\tau$ שציפינו
לקבל בהתאמה לגרף \ref{fig:amp-omega} אמור להתאים ל-$\tau$ שהתקבל מההתאמה לגרף
הדעיכה \ref{fig:tau}, שהתקבל כאמור עבור מערכת א'.  הפער בין שני ערכי $\tau$
אלה של מערכת א', הוא $0.28 \pm 0.03 [s]$. סך הכל היו ברשותינו 18 נקודות מ-18 מדידות שונות, אליהן התאמנו את הגרף ב-\ref{fig:amp-omega}. זו לא כמות מספיק גדולה בשביל לאפיין את המערכת כראוי. אם היינו מבצעים יותר מדידות, בתדירויות שונות, היינו אולי מצליחים להתקרב יותר ל-$\tau$ שהתקבל בדעיכה - גרך \ref{fig:tau}. אבל בהתחשב בסדרי הגודל של $\tau$ ובזמן שהיה ברשותנו בעת ביצוע המדידות, פער זה מספיק קטן בשביל לאמת את המודל התאורטי במידת מה.

לפי המודל התאורטי, $\omega_0 = 6.924\pm0.005 [rad/s]$ ואילו בהתאמה לגרף
\ref{fig:amp-omega}, קיבלנו $\omega_0 = 6.856 \pm 0.001 [rad/s]$. פער זה קטן מאוד
ומצביע על הצלחה של המודל התאורטי מבחינת פרמטר זה של המערכת.

לעומת זאת, בהתאמה לגרף \ref{fig:phase-omega}, קיבלנו ערך התאמה ל-$\tau$, שהיה
דווקא רחוק מערכו של $\tau$ שהתקבל במדידה דומה למדידה המוצגת בגרף \ref{fig:tau},
(שנעשתה עבור מערכת ב'). יכול להיות שפער זה התקבל בגלל שחישוב הפרשי הפאזה שלנו
לא היה מדויק, והוא נעשה באופן ידני לכל מדידה. אין בידינו את הניסיון ו/או הכלים
לבצע חישוב כזה מורכב, באופן שיתאים לכל המדידות. החישוב נעשה על ערכים שהתקבלו
בהתאמות למדידת המיקום \emph{עבור כל תדירות}.

נתייחס כעת למקדם האיכות של מערכת א', לפי הגדרה \ref{eq:Q-tau} ולפי הגדרה
\ref{eq:Q-delta-omega}. עבור ה-$\omega_0$ שמתקבלת ממדידת קבועי הקפיצים והמסה,
וה-$\tau$ שהתקבל במדידה \ref{fig:tau}, מתקבל $Q = 23.409$\footnote{לערך זה אין
שגיאה כי ערכו התקבל בין היתר משימוש בערך של $\tau$ שהתקבל בהתאמה לגרף שלה אין
לנו ערכי שגיאה.}, לעומת זאת, מחישוב $\Delta \omega$ לערכי ההתאמה בגרף
\ref{fig:amp-omega}, מתקבל ש-$Q = 25.128\pm0.002$.\footnote{למעשה פתרנו את המשוואה $A_{max}(\omega_{1,2}) =
	\frac{1}{\sqrt{2}} \cdot A_{max}(\omega_0)$ וחישבנו את ההפרש בין שני
	הפתרונות שלה, $\omega_1$ ו-$\omega_2$. חישבנו את השגיאה ל-$Q$ באמצעות גזירה של הביטוי שהתקבל עבור
		$\Delta\omega$, לפי $\omega_0$ ולפי $\tau$ ומכפלת כל נגזרת בשגיאות
		שהתקבלו בהתאמות ל-$\tau$ ול-$\omega_0$, בהתאמה. אפשר לראות את החישובים
	במחברת ה-\href{https://www.wolfram.com/mathematica/}{Mathematica}
ה\href{https://gitlab.com/doronbehar/physics1m-lab-harmonic-oscillator/-/blob/master/DRIVEN-DAMPED/delta-omega.nb}{זו}.}
פער זה אמנם בסדר גודל של $10\%$ אבל זו עדיין עדות לכך שהמדידות וההתאמות היו די מדויקות.

\subsection{שגיאות המדידה}

מכשיר המדידה היחיד שלקח חלק במדידות היה החיישן הפוטואלקטרי. הוא ספר כמה שיניים
ימינה ושמאלה העגלה עברה, בתדר דגימה מסוים. בכל המדידות תדר דגימה היה 200 דגימות
לשנייה. בנוסף הייתה לו שגיאה של $\pm 1$ שיניים ימינה או שמאלה.\footnote{ניתן
	לקרוא דיון מעניין
	\href{https://discourse.julialang.org/t/julia-vs-gnu-octave-for-plot-fitting-finding-peaks/42237}{כאן}
	על ההתמודדות עם רזולוציה זו של החיישן שהוכיחה עצמה כנמוכה מדי עבור משרעות
מסוימות.} לכן סך הכל השגיאה שלו היא $\pm 1$ שיניים במדידת המשרעת של המאלץ והמסה, ו-$\pm0.0005[s]$ ($\pm
0.001 [s]$ במדידת הזמן.  בגרף \ref{fig:tau} המציג באופן ישיר את המדידות של החיישן, לא ניתן היה להציג
רווח בר סמך כי גודלו היה קטן מידי. בגרפים \ref{fig:amp-omega}
ו-\ref{fig:phase-omega}, גם כן לא ניתן היה להציג רווח בר סמך כי כל נקודה הייתה
ערך של התאמה שהתקבל מבלי שגיאות.\footnote{השתמשנו בפונקציה \texttt{leasqr}
	שאיננה יודעת להציג ערכי שגיאה לכל פרמטר (למיטב הבנתנו), לפי התיעוד
\href{https://octave.sourceforge.io/optim/function/leasqr.html}{כאן}.}

\section{מסקנות}

בניסוי חקרנו את תופעת התהודה במקרה בו קיים ריסון. התמונה שהתקבלה עבור הפאזה
והאמפליטודה של המסה, כתלות בתדירות מייצגות במידה טובה את הציפיות אך עם סטיות
קלות. המודל התאורטי ניבא די טוב שהמסה תנוע באמפליטודה הגבוהה ביותר סביב תדר
התהודה ועם הפרש פאזה יחסית קרוב ל-$\frac{\pi}{2}$ סביב תדר התהודה. לעומת זאת,
המאפיינים של המערכת כמו $\tau$, $\omega_0$ ו-$Q$, לא היו קרובים לתאוריה כפי
שציפינו. אף על פי כן, בהתחשב בכמות המדידות שעשינו, ובכלים החישוביים שברשותנו,
התוצאות מניחות את הדעת.

\section{נספח}

הקוד שנכתב לצורך עיבוד הנתונים, חישוב השגיאות, והנתונים עצמם נמצאים
\href{https://gitlab.com/doronbehar/physics1m-lab-harmonic-oscillator/blob/master/DRIVEN-DAMPED}{כאן}.

\end{document}
