global cm
cm = struct();
function plotM(m)
	% scope
	global steady_measurements
	if class(m) == "double"
		fname = sprintf("measurements/%s-%d.txt", "steady", m)
		m = steady_measurements(strcmp({steady_measurements.fname}, fname));
		if size(m, 2) != 1
			warning("No such measurement was indexed")
			return
		end
	end
	% scope
	global cm
	cm = m;
	hold on;
	if strcmp(m.type, "steady")
		plot(m.mass.times, m.mass.counts, '.');
		plot(m.mass.times, m.fit.computed_values.mass, '-');
		plot(m.drive.times, m.drive.counts, '.');
		plot(m.drive.times, m.fit.computed_values.drive, '-');
		lgd = { ...
			"mass", ...
			"mass fit", ...
			"drive", ...
			"drive fit" ...
		};
	elseif strcmp(m.type, "tau")
		plot(m.times, m.counts, '.');
		plot(m.times, m.fit.computed_values, '-');
		lgd = { ...
			"mass", ...
			"mass fit", ...
		};
	end
	xlabel("Measurement Time [s]");
	ylabel("offset of teeths");
	legend(lgd, ...
		'Location', "north" ...
	)
	legend('boxoff')
end
